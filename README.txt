Chained drop down is a very simple/lightweight Chained Select/Dropdown widget
for taxonomy term reference field.

How to use:
1) install the module;
2) add a taxonomy term reference field to any entity, choose "select list" as
widget;
3) chained select will be automatically apply to the multi-level vocabulary.
